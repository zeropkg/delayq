package delayq

const (
	TIME_FORMAT = "2006-01-02 15:04:05"
	WHEEL_SIZE  = 3600
)

const (
	taskInQueue   = "task_in_queue"
	taskInExecute = "task_in_execute"
)
