package delayq

import (
	"fmt"
	"time"

	recursive_deep_hash "github.com/panospet/recursive-deep-hash"
	"github.com/pkg/errors"
)

func TimeString(times ...time.Time) string {
	if len(times) > 0 {
		return times[0].Format(TIME_FORMAT)
	}
	return time.Now().Format(TIME_FORMAT)
}

func PrintCurrentTime() {
	fmt.Println(TimeString())
}

func getExeBuilderHash(exeBuilder ExeBuilder) (hash string, err error) {
	var ans string
	ans, err = recursive_deep_hash.ConstructHash(exeBuilder)
	if err != nil {
		err = errors.Wrap(err, "generate recursive_deep_hash failed")
		return
	}
	hash = ans[0:8]
	return
}
