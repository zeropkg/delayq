package delayq

import (
	"context"
	"fmt"
	"runtime"
	"strings"
)

func Goroutine(ctx context.Context, f func()) {
	defer Recover(ctx)
	f()
}

func Recover(ctx context.Context) {
	err := recover()
	if err != nil {
		stackInfo := ""
		for i := 2; i < 64; i++ {
			_, file, line, ok := runtime.Caller(i)
			if ok {
				stackInfo += fmt.Sprintf("%d:%s#%d ", i-1, file, line)
			} else {
				break
			}
		}

		if stackInfo != "" {
			Logger.Fatal("panic recover, the stack info is ", strings.TrimSpace(stackInfo))
		}
	}
}
