package delayq

import (
	"context"
	"fmt"

	"gitlab.com/zeropkg/itimer"
)

type Task struct {
	// 原始输入相关
	TaskID      string      //任务ID，由业务层自己保证它的唯一性
	TaskInput   interface{} //任务方法参数
	delaySecond uint
	timeGroup   *itimer.TimerGroup //和计时相关的
	// inQueueTime time.Time
	// executeTime *time.Time

	// 控制参数，是在任务创建的时候就已经明确的
	CycleCount    int  //任务在时间轮上的循环次数，等于0时，执行该任务
	WheelPosition uint //任务在时间轮上的位置

	// Task本身是单链表形式，指向下一个任务的指针Next
	Next *Task
}

// newTask ...
func newTask(taskID string, input interface{}, delaySecond uint, cycleCount int, wheelPosition int) *Task {
	tg := itimer.NewTimerGroup()
	defer tg.Start(taskInQueue)

	task := &Task{
		TaskID:        taskID,              // 任务ID
		TaskInput:     input,               // 任务原始输入，是个interface{}
		CycleCount:    cycleCount,          // 时间轮圈数
		WheelPosition: uint(wheelPosition), // 时间轮槽位
		delaySecond:   delaySecond,         // 延迟的秒数
		timeGroup:     tg,                  // 每个任务都有一个timegroup
	}
	return task
}

func (task *Task) execute(ctx context.Context, dq *DelayQueue) {
	// 1. 停止inQueue的时间
	task.timeGroup.Stop(taskInQueue)

	// 2. 进行延迟执行
	task.timeGroup.Start(taskInExecute)
	err := dq.IExecute.DelayDo(task.TaskInput)
	task.timeGroup.Stop(taskInExecute)

	partInfo := fmt.Sprintf("[dqHash]%v [taskID]%v [delaySecond]%v [inQueue]%v [inExecute]%v [tailTime]%v",
		dq.hashID,
		task.TaskID,
		task.delaySecond,
		task.timeGroup.GetTime(taskInQueue).String(),
		task.timeGroup.GetTime(taskInExecute).String(),
		TimeString(),
	)
	if err != nil {
		Logger.Errorf("DelayDo failed, %v, [error]%v [input]%v",
			partInfo,
			err,
			task.TaskInput,
		)
	} else {
		if EnableExecuteInfo {
			Logger.Infof("DelayDo success, %v",
				partInfo,
			)
		}
	}

}
