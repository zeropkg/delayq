package main

import (
	"errors"
	"fmt"
	"time"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/zeropkg/delayq"
)

// Demo
type Demo struct{}

// Demo 用到的数据Data
type Data struct {
	Name        string
	Age         int
	DelaySecond int
}

// Demo 作为delayq.IExecute的实现，比如实现DelayDo方法
func (demo *Demo) DelayDo(input interface{}) (err error) {
	fmt.Println("------------demo延迟执行", delayq.TimeString())
	switch inputData := input.(type) {
	case *Data:
		fmt.Printf("type is *Data,name=%v\n", inputData.Name)
	case Data:
		fmt.Printf("type is Data,name=%v\n", inputData.Name)
	case string:
		fmt.Printf("type is string,input=%v\n", inputData)
	case *delayq.DelayQueue:
		fmt.Printf("type is *delayq.DelayQueue")
		dq := input.(*delayq.DelayQueue)
		dq.Push("temp", input, 2)

	default:
		err = errors.New("not handle the input type")
	}
	return
}

// Demo相当于接口delayq.IExecute的实现，而这个构造方法签名是func() delayq.IExecute， 可以叫别名ExeBuilder
func NewDemo() delayq.IExecute {
	return &Demo{}
}

///////////////

type Demo1 struct{}

func (demo1 *Demo1) DelayDo(input interface{}) error {
	fmt.Println("------------demo1延迟执行", delayq.TimeString())
	time.Sleep(4 * time.Second)
	return nil
}

func NewDemo1() delayq.IExecute {
	return &Demo1{}
}

func main() {
	fmt.Println("------------开始执行", delayq.TimeString())
	dq, _ := delayq.CreateDelayQueue(NewDemo)

	// data := &Data{Name: "tony", Age: 6}

	dq.Push("hello1", dq, 7)

	<-make(chan struct{})
}

func main4() {
	fmt.Println("------------开始执行", delayq.TimeString())
	dq, _ := delayq.CreateDelayQueue(NewDemo)

	data := &Data{Name: "tony", Age: 6}

	dq.Push("hello1", data, 1)
	dq.Push("hello2", data, 2)
	dq.Push("hello3", data, 3)

	fmt.Println(dq.CancelAll())
	spew.Dump(dq.ListAll())

	dq.Push("hello4", data, 3)

	<-make(chan struct{})

}

func main2() {
	fmt.Println("------------开始执行", delayq.TimeString())
	dq, _ := delayq.CreateDelayQueue(NewDemo)

	data := &Data{Name: "tony", Age: 6}
	dq.Push("1", data, 1)

	dq.Push("2", "to cancel1", 2)
	dq.Push("2", "to cancel2", 3)
	dq.Push("3", "hello", 3)
	dq.Push("2", "to cancel3", 3)
	dq.Push("4", 1, 4)

	cancelCount := dq.Cancel("2")
	spew.Config.Dump(cancelCount)
	dq1, _ := delayq.CreateDelayQueue(NewDemo1)
	dq1.Push("01", 123, 6)

	<-make(chan struct{})
}

func main1() {
	fmt.Println("------------开始执行", delayq.TimeString())
	dq, _ := delayq.CreateDelayQueue(NewDemo)
	spew.Dump(len(dq.TimeWheel))
	<-make(chan struct{})

}
