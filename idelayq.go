package delayq

import "time"

type IDelayQ interface {
	Push(delaySecond time.Duration, taskKey string, taskParams string) error
}
