package delayq

import (
	"sync"
	"time"
)

// dqMap key是hash string，value是时间轮盘指针
var dqMap map[string]*DelayQueue = make(map[string]*DelayQueue)

// DelayQueue
// 1. 一个DelayQueue就是单纯一个类型的“队列时钟”;
// 2. 每个队列时钟有3600个槽位（60分*60秒=3600秒），每个槽位上都可以挂载一个任务单链表；
// 3. 每秒只有一个槽位被轮询到，找到这个槽位所挂载的所有任务，每个任务都有一个重要的属性就是CycleCount。当CycleCount是0的时候，任务应该执行，然后删除；当CycleCount不是0的时候要递减一。
type DelayQueue struct {
	// ExeBuilder           // 任务工厂方法指针, 需要在对象创建时初始化它
	IExecute            // ExeBuilder() 返回的对象，需要在对象创建时初始化它
	onceStart sync.Once // 一个延迟队列只能启动一次

	WheelPosition uint              // 时间轮当前处理的槽位
	TimeWheel     [WHEEL_SIZE]*Task // 时间轮，定长的循环队列
	hashID        string
	taskMap       map[string][]*Task // key是taskID，value是[]*Task
	sync.RWMutex                     // 当前延迟队列的读写锁
}

// createDelayQueue 1.启动并返回延迟队列的实例 2.将延迟队列加入到全局map中
func createDelayQueue(hashID string, executor IExecute) *DelayQueue {
	dqInstance := &DelayQueue{
		IExecute: executor,
		hashID:   hashID,
		taskMap:  make(map[string][]*Task),
	}
	dqInstance.start()
	return dqInstance
}

// CreateDelayQueue 创建延迟队列的对象
// exeBuilder 当前IExecute具体实现的构造方法指针
// 根据传入的ExeBuilder计算出hash，如果hash已经存在直接返回延迟队列实例，如果不存在就新建
func CreateDelayQueue(exeBuilder ExeBuilder) (*DelayQueue, error) {
	hash, err := getExeBuilderHash(exeBuilder)
	if err != nil {
		return nil, err
	}
	// 如果已存在，直接从dqMap中返回
	if dqInstance, yes := dqMap[hash]; yes {
		return dqInstance, nil
	}
	// 如果不存在，新创建轮盘对象并塞入到dqMap中
	dqInstance := createDelayQueue(hash, exeBuilder())
	dqMap[hash] = dqInstance

	return dqInstance, nil
}

// Cancel 取消任务
func (dq *DelayQueue) Cancel(taskID string) (count int) {
	count = dq.pop(taskID)
	return
}

// CancelAll 删除全部任务
func (dq *DelayQueue) CancelAll() (count int) {
	for taskID := range dq.taskMap {
		count += dq.pop(taskID)
	}
	return count
}

// List 根据传入的taskIDs列出任务列表
func (dq *DelayQueue) List(taskIDs ...string) (result map[string][]*Task) {
	dq.RLock()
	defer dq.RUnlock()

	result = make(map[string][]*Task)
	// 按需查询的情况
	if len(taskIDs) > 0 {
		for _, taskID := range taskIDs {
			var ok bool
			if result[taskID], ok = dq.taskMap[taskID]; !ok {
				result[taskID] = []*Task{}
			}
		}
		return
	}
	// 全量查询的情况
	result = dq.taskMap
	return
}

// ListAll 查询全部的任务列表
func (dq *DelayQueue) ListAll() (result map[string][]*Task) {
	return dq.taskMap
}

func (dq *DelayQueue) push(task *Task) {
	dq.pushTimeWheel(task)
	dq.pushTaskMap(task)
}

// pushTaskMap push任务到TaskMap中
func (dq *DelayQueue) pushTaskMap(task *Task) {
	dq.Lock()
	defer dq.Unlock()

	taskID := task.TaskID
	_, yes := dq.taskMap[taskID]
	if yes {
		dq.taskMap[taskID] = append(dq.taskMap[taskID], task)
	} else {
		dq.taskMap[taskID] = []*Task{task}
	}
}

func (dq *DelayQueue) pushTimeWheel(task *Task) {
	// todo 以后优化，目前这种写法可能在并发场景涉及到覆盖
	if dq.TimeWheel[task.WheelPosition] == nil {
		dq.TimeWheel[task.WheelPosition] = task
	} else {
		head := dq.TimeWheel[task.WheelPosition]
		task.Next = head
		dq.TimeWheel[task.WheelPosition] = task
	}
}

func (dq *DelayQueue) pop(taskID string) (count int) {
	count = dq.popTaskMap(taskID)
	return
}

func (dq *DelayQueue) popTimeWheel(wheelPosition uint, taskID string) {
	previous, current := dq.TimeWheel[wheelPosition], dq.TimeWheel[wheelPosition]
	popLogFunc := func(task *Task) {
		Logger.Infof("pop task success, [dqHash]%v [wheelPosition]%v [taskID]%v [delaySecond]%v ",
			dq.hashID,
			wheelPosition,
			task.TaskID,
			task.delaySecond,
		)
	}
	for current != nil {
		if current.TaskID == taskID {
			if previous == current {
				// 如果是第一个节点，队列不断的收缩
				dq.TimeWheel[wheelPosition] = current.Next
				previous = current.Next
			} else {
				// 如果previous不等于current，其实就是前后指针已经分离了，那么要跳过当前的节点，把current.Next赋值给previous.Next
				previous.Next = current.Next
			}
			popLogFunc(current)
		} else {
			previous = current
		}
		current = current.Next
	}
}

// popFromTaskMap 根据taskID从TaskMap中移除
func (dq *DelayQueue) popTaskMap(taskID string) (count int) {
	dq.Lock()
	defer dq.Unlock()

	tasks, yes := dq.taskMap[taskID]
	if yes {
		slotMap := make(map[uint]struct{})
		for _, task := range tasks {
			slotMap[task.WheelPosition] = struct{}{}
		}
		// 当前时间轮槽位遍历整个队列，根据taskID删除task
		for wheelPosition, _ := range slotMap {
			dq.popTimeWheel(wheelPosition, taskID)
		}

		count += len(tasks)
		delete(dq.taskMap, taskID)
	}

	return
}

// winding 上发条，返回圈数和位置
func (dq *DelayQueue) winding(delaySecond uint) (cycleCount int, wheelPosition int) {
	mileage := int(dq.WheelPosition + delaySecond) // 里程，当前位置加上延迟秒数
	cycleCount = mileage / WHEEL_SIZE              // 圈数，
	// wheelPosition = mileage%WHEEL_SIZE - 1      // 位置，因为是从0开始的所以要减去1，注释掉这里不对，-1会越界的
	wheelPosition = mileage % WHEEL_SIZE
	return
}

// Push
func (dq *DelayQueue) Push(taskID string, input interface{}, delaySecond uint) error {
	cycleCount, wheelPosition := dq.winding(delaySecond) // 根据延迟的秒来计算圈数和位置
	task := newTask(taskID, input, delaySecond, cycleCount, wheelPosition)
	dq.push(task)
	Logger.Infof("push task success, [dqHash]%v [wheelPosition]%v [taskID]%v [delaySecond]%v ",
		dq.hashID,
		wheelPosition,
		task.TaskID,
		task.delaySecond,
	)
	return nil
}

// Start 整个时间轮盘的启动
func (dq *DelayQueue) Start() {
	dq.onceStart.Do(dq.start)
}

// rotate 轮转
func (dq *DelayQueue) rotate() {
	dq.WheelPosition++
	if dq.WheelPosition >= WHEEL_SIZE {
		dq.WheelPosition = dq.WheelPosition % WHEEL_SIZE
	}
}

func (dq *DelayQueue) start() {
	// 1. 时间轮的操作、新起一个goroutine
	go Goroutine(ctx, func() {
		for {
			select {
			case <-time.After(time.Second * 1):
				previous, current := dq.TimeWheel[dq.WheelPosition], dq.TimeWheel[dq.WheelPosition]
				for current != nil {
					if current.CycleCount == 0 {
						// 2. 新起goroutine进行方法的调度执行
						current := current

						go Goroutine(ctx, func() {
							current.execute(ctx, dq)
						})

						if previous == current {
							// 如果是第一个节点，队列不断的收缩
							dq.TimeWheel[dq.WheelPosition] = current.Next
							previous = current.Next
						} else {
							previous.Next = current.Next
						}
					} else {
						current.CycleCount--
						previous = current
					}

					current = current.Next
				}
				// 槽位指向下一个
				dq.rotate()
				// dq.WheelPosition++
			}
		}
	})
}
