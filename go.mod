module gitlab.com/zeropkg/delayq

go 1.20

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/panospet/recursive-deep-hash v0.0.0-20210223103211-bd131c485fab
	github.com/pkg/errors v0.9.1
	gitlab.com/zeropkg/itimer v1.0.0
)
