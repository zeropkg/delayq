package delayq

// IExecute 输入参数是interface{}
type IExecute interface {
	DelayDo(input interface{}) error
}

// ExeBuilder 是func() IExecute的别名，是指向方法的指针.
type ExeBuilder func() IExecute
