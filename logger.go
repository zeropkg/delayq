package delayq

import (
	"encoding/json"
	"fmt"
	"log"
)

const (
	levelInfo  = "info"
	levelWarn  = "warn"
	levelError = "error"
	levelFatal = "fatal"
)

type ILogger interface {
	Info(args ...interface{})
	Warn(args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})

	Infof(format string, args ...interface{})
	Warnf(format string, args ...interface{})
	Errorf(format string, args ...interface{})
	Fatalf(format string, args ...interface{})
}

type defaultLogger struct {
}

func newDefaultLogger() *defaultLogger {
	log.SetFlags(0)
	return &defaultLogger{}
}

func (l *defaultLogger) Info(args ...interface{}) {
	l.jsonPrint(map[string]interface{}{
		"level": levelInfo,
		"msg":   fmt.Sprint(args...),
	})
}

func (l *defaultLogger) Warn(args ...interface{}) {
	l.jsonPrint(map[string]interface{}{
		"level": levelWarn,
		"msg":   fmt.Sprint(args...),
	})
}

func (l *defaultLogger) Error(args ...interface{}) {
	l.jsonPrint(map[string]interface{}{
		"level": levelError,
		"msg":   fmt.Sprint(args...),
	})
}

func (l *defaultLogger) Fatal(args ...interface{}) {
	l.jsonPrint(map[string]interface{}{
		"level": levelFatal,
		"msg":   fmt.Sprint(args...),
	})
}

func (l *defaultLogger) Infof(format string, args ...interface{}) {
	l.jsonPrint(map[string]interface{}{
		"level": levelInfo,
		"msg":   fmt.Sprintf(format, args...),
	})
}

func (l *defaultLogger) Warnf(format string, args ...interface{}) {
	l.jsonPrint(map[string]interface{}{
		"level": levelWarn,
		"msg":   fmt.Sprintf(format, args...),
	})
}

func (l *defaultLogger) Errorf(format string, args ...interface{}) {
	l.jsonPrint(map[string]interface{}{
		"level": levelError,
		"msg":   fmt.Sprintf(format, args...),
	})
}

func (l *defaultLogger) Fatalf(format string, args ...interface{}) {
	l.jsonPrint(map[string]interface{}{
		"level": levelFatal,
		"msg":   fmt.Sprintf(format, args...),
	})
}

func (l *defaultLogger) jsonPrint(params map[string]interface{}) {
	params["mod"] = "delayq"
	bytes, err := json.Marshal(params)
	if err != nil {
		return
	}
	log.Printf("%s", bytes)
}
