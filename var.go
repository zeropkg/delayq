package delayq

import "context"

var (
	ctx               = context.Background()
	EnableExecuteInfo = true // 是否打开执行信息的开关
)
