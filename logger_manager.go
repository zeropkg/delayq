package delayq

var (
	Logger = newLoggerManager()
)

type LoggerManager struct {
	enable bool
	logger ILogger
}

func newLoggerManager() *LoggerManager {
	return &LoggerManager{
		enable: true,
		logger: newDefaultLogger(),
	}
}

func (m *LoggerManager) printLog(logFunc func(...interface{}), args ...interface{}) {
	if !m.enable || logFunc == nil {
		return
	}
	logFunc(args...)
}

func (m *LoggerManager) printfLog(logFunc func(string, ...interface{}), format string, args ...interface{}) {
	if !m.enable || logFunc == nil {
		return
	}
	logFunc(format, args...)
}
func (m *LoggerManager) SetLogger(l ILogger) {
	m.logger = l
}

func (m *LoggerManager) Enable(isEnable bool) {
	m.enable = isEnable
}

func SetLogger(l ILogger) {
	Logger.SetLogger(l)
}

func LogEnable(isEnable bool) {
	Logger.Enable(isEnable)
}

func (m *LoggerManager) Info(args ...interface{}) {
	m.printLog(m.logger.Info, args...)
}

func (m *LoggerManager) Warn(args ...interface{}) {
	m.printLog(m.logger.Warn, args...)
}

func (m *LoggerManager) Error(args ...interface{}) {
	m.printLog(m.logger.Error, args...)
}

func (m *LoggerManager) Fatal(args ...interface{}) {
	m.printLog(m.logger.Fatal, args...)
}

func (m *LoggerManager) Infof(format string, args ...interface{}) {
	m.printfLog(m.logger.Infof, format, args...)
}

func (m *LoggerManager) Warnf(format string, args ...interface{}) {
	m.printfLog(m.logger.Warnf, format, args...)
}

func (m *LoggerManager) Errorf(format string, args ...interface{}) {
	m.printfLog(m.logger.Errorf, format, args...)
}

func (m *LoggerManager) Fatalf(format string, args ...interface{}) {
	m.printfLog(m.logger.Fatalf, format, args...)
}
